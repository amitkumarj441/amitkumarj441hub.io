==================================================================
https://keybase.io/amitkumarjaiswal
--------------------------------------------------------------------

I hereby claim:

  * I am an admin of https://amitkumarj441.github.io
  * I am amitkumarjaiswal (https://keybase.io/amitkumarjaiswal) on keybase.
  * I have a public key with fingerprint 2E5D 9779 AD08 5EE7 AC29  DC41 EBE7 39F0 0427 4A2C

To do so, I am signing this object:

{
    "body": {
        "key": {
            "eldest_kid": "010127fa8cd4a6d3c1ccba771905fc8204ef2783c82ac20565a6ffb8f14001f74e1a0a",
            "fingerprint": "2e5d9779ad085ee7ac29dc41ebe739f004274a2c",
            "host": "keybase.io",
            "key_id": "ebe739f004274a2c",
            "kid": "010127fa8cd4a6d3c1ccba771905fc8204ef2783c82ac20565a6ffb8f14001f74e1a0a",
            "uid": "54f386f464201cf74e356647def20619",
            "username": "amitkumarjaiswal"
        },
        "revoke": {
            "sig_ids": [
                "1cfa0924908ed8e5c9fe548a511170c52fbd3be4656fb9e306e6e6291668e6d10f"
            ]
        },
        "service": {
            "hostname": "amitkumarj441.github.io",
            "protocol": "https:"
        },
        "type": "web_service_binding",
        "version": 1
    },
    "ctime": 1471686122,
    "expire_in": 157680000,
    "prev": "dc75a34cd06098bf66c08602f07d51b7ffd07b5e32685b88eba7e971d0d6a587",
    "seqno": 6,
    "tag": "signature"
}

which yields the signature:

-----BEGIN PGP MESSAGE-----
Version: Keybase OpenPGP v2.0.56
Comment: https://keybase.io/crypto

yMJ2AnicrZJbbBVFHMZPbaXSRDDEQBCIOkgpoTmZ2Z3bHpMKCUpN0IQXIQF6nJ1L
u72cPd3d02OtfUFSIPWBhOgLkgDWGEIIhtRYroFybCQ1mojxQgIargGSxgYJFlN0
tuiTPjrzMHv5vt//m/9M5cnqTF3V0tf/qHlj4Mi8qvHRWaXMhpFlt/uAH6pekOsD
HXpm0Z1Kx0m+I1AgByCCyGFGcKmwoMqVSEpfMIY8SIzkDsTaOIy79lFIBxJKBDXG
5wZhCJFhWCMBBWgEJii06qgYBYXEYh1NlMeYJxTkRGtmvZ6SGGlfM9czEGKHYeFI
a2wL49Rhw/ki1tkgtN/sS34m3n/o/+fcpRkcwcbl1GCKHYhk+tsllGKmLAVS5KXC
WEcF0aWtWnQFSUepS0TtIojLohP0N4JI94QdOu1vHLTa8DHIbQIWJaDnYA9yrbgm
0jOaYC4IQohBSRzjK9fXmBJqfE+7kGo7HQ9RyjVVCBqwxbJt5Z5AzsDTbv0rBcYo
2xokbSX/UfuKUZiEMuy0orYkKca5NGDSW0xdZe3n/+bl/aCg7LFZR4+O4iAsgByy
SpkEaQGEGaKcIsdpBPqtYhDpfJAqCKMc2pHW0T0WqSQjwsVSQQo97htKJeQUOgYy
RZDPjFGQ+US7DuXE51zbg9IeQwoqKghnIN1hdyEEOWpjilaLtD0siKQUadA/emZz
TaaqLjPr8cfS65ypm/3UP5d8dUvtwxfivcnur8buZ+bgtq9PT7+TqR37cc6x7S81
jy+5/OyNPWPlwbtPDH9Lzu6qyrbvO7/i6O8f/jy5aeNQ99L3H7TXLj830RTDSj2G
35xsunRw8dXvntu5BgwtAAemv/9i2+Cpe1ca7z5cWB+ca/5160RD9ZINNc/0T76y
cs3ce+OwZfe6M5nktwXThy5cjG4WKy/C0anZxOsfqfzw8qHlh6+XV6wa5qeynQ2V
g78surVqW/1+PJkvLxP3F58+fHxn042tlff29B35tPfisRM7LmTXDmw2R6/m123f
dQXQzz7+6NU7LVPV3p9z3x0Yejp6c/0nX/401TDY/PwH3bcqK98enn+iumtspDxx
7bXP/wI9oGXp
=8orD
-----END PGP MESSAGE-----

And finally, I am proving ownership of this host by posting or
appending to this document.

View my publicly-auditable identity here: https://keybase.io/amitkumarjaiswal

==================================================================
